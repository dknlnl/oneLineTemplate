#ifndef ONELINETEMPLATE_H

#define ONELINETEMPLATE_H

#define CONSTTEXTLEN 256

typedef enum {
    DATATYPE_CONSTTEXT,
    DATATYPE_VARINDEX
} OneLineTemplateDataType;

typedef struct OneLineTemplate {
    OneLineTemplateDataType dataType;
    union
    {
        struct
        {
            size_t varIndex; //1 is the first valid varIndex, 0 is reserve for undefined var
            char   varName[CONSTTEXTLEN-4];
        }var;
        char   constText[CONSTTEXTLEN];
    }data;
    struct OneLineTemplate *next;
} OneLineTemplate;

typedef struct {
    const char *varName;
    char        varValue[128];
}SymTable;

OneLineTemplate* OneLineTemplateCreate(const char* text, const SymTable *symTable, size_t symTableLen);
void OneLineTemplateRelase(OneLineTemplate *tpl);
int  OneLineTemplateExpand(const OneLineTemplate *tpl, const SymTable *symTable, size_t symTableLen, char *expandText, size_t expandTextLen);
int  OneLineTemplateVarify(const OneLineTemplate *tpl, const SymTable *symTable, size_t symTableLen, const char *text);

#endif /* end of include guard: ONELINETEMPLATE_H */
