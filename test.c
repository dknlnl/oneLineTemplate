#include <stdlib.h>
#include <stdio.h>
#include "OneLineTemplate.h"

int main(int argc, const char *argv[])
{
    char text[1024];
    const SymTable symTable[] = 
    {
        {"_UNDEFINED", ""},
        {"foo"   , "12345566"},
        {"bar" , "1000"},
    };

    OneLineTemplate * tpl = OneLineTemplateCreate("Hello{foo}and{bar}abv", symTable, sizeof(symTable)/sizeof(symTable[0]));
    if (NULL == tpl)
    {
        fprintf(stderr, "pares fail\n");
        return 1;
    }

    OneLineTemplateExpand(tpl, symTable, sizeof(symTable)/sizeof(symTable[0]), text, sizeof(text));
    fprintf(stdout, "%s\n", text);
    OneLineTemplateRelase(tpl);
    return 0;
}
