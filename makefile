test.exe: OneLineTemplate.o test.o
	gcc -g $? -o $@ 
	rm $?

OneLineTemplate.o:OneLineTemplate.c
	gcc -g -c $? -o $@ -std=c99 -I "include/micro" -DNODEBUG

test.o:test.c
	gcc -g -c $? -o $@ -std=c99 -I "include/micro" -DNODEBUG
