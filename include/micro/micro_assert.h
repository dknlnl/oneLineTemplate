#ifndef MICRO_CHECKPARA_H

#define MICRO_CHECKPARA_H

#define UASSERT(__exp__, __fallback__) \
{\
    if (!(__exp__)) {\
        le("assertion '%s' fail!", #__exp__)\
        __fallback__;\
    }\
}
#define UASSERT2(__exp__, __ret__)                UASSERT(__exp__, {Return __ret__;})

#define AssertEQFB(__var__, __expect__, __fallback__)  UASSERT((__var__ == __expect__), __fallback__)
#define AssertNEFB(__var__, __expect__, __fallback__)  UASSERT((__var__ != __expect__), __fallback__)
#define AssertEQ(__var__, __expect__, __ret__)         AssertEQFB(__var__, __expect__, {Return __ret__;})
#define AssertNE(__var__, __expect__, __ret__)         AssertNEFB(__var__, __expect_, {Return __ret__;})


#endif /* end of include guard: MICRO_CHECKPARA_H */
