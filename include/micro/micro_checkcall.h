#ifndef MICRO_CHECKCALL_H

#define MICRO_CHECKCALL_H

#define CheckCall(__call__, __ret__, __fallback__) \
{\
    ld("Call  : '%s'", #__call__);\
    __ret__ = (__call__);\
    if (0 != __ret__) \
    {\
        le("Fail  : '%s'", #__call__);\
        __fallback__;\
    }\
    else \
    {\
       ld("Succes: '%s'", #__call__) ;\
    }\
}
#define CC(__call__,  __ret__, __fallback__) CheckCall(__call__, __ret__, __fallback__)


#define CheckNotNullCall(__call__,  __ret__, __fallback__)\
{\
    ld("Call  : '%s'", #__call__);\
    __ret__ = (__call__);\
    if (NULL == __ret__) \
    {\
        le("Fail  : '%s'", #__call__);\
        __fallback__;\
    }\
    else \
    {\
       ld("Succes: '%s'", #__call__) ;\
    }\
}
#define CNNC(__call__, __ret__, __fallback__) CheckNotNullCall(__call__, __ret__, __fallback__)

#define ReturnFB(ret) {Return ret;}
#define DefaultFB ReturnFB(-1)
#define ReturnVoidFB {Return;}
#define NullFB    {}


#endif /* end of include guard: MICRO_CHECKCALL_H */
