#ifndef MICRO_LOG_H

#define MICRO_LOG_H
#include <time.h>

#define li(fmt, ...)\
{\
    time_t t = time( NULL );   \
    char tmpBuf[20];   \
    strftime(tmpBuf, sizeof(tmpBuf), "%Y%m%d %H:%M:%S", localtime(&t));\
    fprintf(stdout, "%s INFO  [%s %d][%s] " fmt "\n", tmpBuf, __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__);\
}

#define le(fmt, ...)\
{\
    time_t t = time( NULL );   \
    char tmpBuf[20];   \
    strftime(tmpBuf, sizeof(tmpBuf), "%Y%m%d %H:%M:%S", localtime(&t));\
    fprintf(stderr, "%s ERROR [%s %d][%s] " fmt "\n", tmpBuf, __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__);\
}

#ifdef NODEBUG
    #define ld(fmt, ...)
#else
    #define ld(fmt, ...)\
    {\
        time_t t = time( NULL );   \
        char tmpBuf[20];   \
        strftime(tmpBuf, sizeof(tmpBuf), "%Y%m%d %H:%M:%S", localtime(&t));\
        fprintf(stdout, "%s DEBUG [%s %d][%s] " fmt "\n", tmpBuf, __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__);\
    }
#endif

#define lfi ld("Enter...")

#define Return \
    ld("Exit.");\
    return

#endif /* end of include guard: MICRO_LOG_H */
