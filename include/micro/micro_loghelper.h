#ifndef MICRO_LOGHELPER_H
#define MICRO_LOGHELPER_H

#define LogVarS(var) ld("%s = %s", #var, var)
#define LogVarI(var) ld("%s = %d", #var, var)
#endif
